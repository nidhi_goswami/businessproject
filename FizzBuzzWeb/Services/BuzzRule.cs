﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzzWeb.Services
{
    public class BuzzRule : IRule
    {
        public bool IsMatch(int number)
        {
            return number % 5 == 0;
        }
        public string Execute()
        {
            return "Buzz";
        }
    }
}