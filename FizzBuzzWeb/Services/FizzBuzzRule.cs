﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzzWeb.Services
{
    public class FizzBuzzRule :IRule
    {
        public bool IsMatch(int number)
        {
            return (number % 3 == 0 && number % 5 == 0);
        }
        public string Execute()
        {
            return "FizzBuzz";
        }
    }
}