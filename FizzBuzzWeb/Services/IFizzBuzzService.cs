﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzzWeb.Services
{
    public interface IFizzBuzzService
    {
        IList<string> GetFizzBuzzNumbers(int input);
    }
}
