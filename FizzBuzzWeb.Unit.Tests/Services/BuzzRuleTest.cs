﻿using System;
using FizzBuzzWeb;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FizzBuzzWeb.Services;
using FluentAssertions;

namespace FizzBuzzWeb.Unit.Tests.Services
{
    public class BuzzRuleTest
    {
        [Theory]
        [InlineData(1, false)]
        [InlineData(5, true)]

        public void IsMatchShouldReturnValueBasedOnInput(int input, bool expectated)
        {
            //Arrange

            var rule = new BuzzRule();

            //Act
            var isMatch = rule.IsMatch(input);

            //Assert
            isMatch.Should().Be(expectated);
        }
        [Fact]
        public void ExecuteShouldReturnValueBasedOnInput()
        {
            //Arrange
            var rule = new BuzzRule();

            //Act
            var execute = rule.Execute();

            //Assert
            execute.Should().Be("Buzz");
        }

    }
}
