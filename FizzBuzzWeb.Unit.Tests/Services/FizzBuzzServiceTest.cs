﻿using Moq;
using FizzBuzzWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FizzBuzzWeb.Services;
using FluentAssertions;

namespace FizzBuzzWeb.Unit.Tests.Services
{
    public class FizzBuzzServiceTest
    {
        private List<IRule> rules;
        private Mock<IRule> ruleFizzBuzz, ruleFizz, ruleBuzz;

        public FizzBuzzServiceTest()
        {
            ruleFizzBuzz = new Mock<IRule>();
            ruleFizz = new Mock<IRule>();
            ruleBuzz = new Mock<IRule>();
            rules = new List<IRule> { ruleFizzBuzz.Object, ruleFizz.Object, ruleBuzz.Object };
        }

        [Fact]
        public void GetFizzBuzzNumber()
        {
            //Arrange
            ruleFizzBuzz.Setup(x => x.IsMatch(15)).Returns(true);
            ruleFizzBuzz.Setup(x => x.Execute()).Returns("FizzBuzz");
            ruleFizz.Setup(x => x.IsMatch(3)).Returns(true);
            ruleFizz.Setup(x => x.Execute()).Returns("Fizz");
            ruleBuzz.Setup(x => x.IsMatch(5)).Returns(true);
            ruleBuzz.Setup(x => x.Execute()).Returns("Buzz");
            var service = new FizzBuzzService(rules);

            //Act
            var number = service.GetFizzBuzzNumbers(5);

            //Assert
            number.Should().BeEquivalentTo(new List<string> { "1", "2", "Fizz", "4", "Buzz" });
        }

        [Fact]
        public void GetErrorMessage()
        {
            //Arrange
            var service = new FizzBuzzService(rules);

            //Act
            var numbers = service.GetFizzBuzzNumbers(-1);

            //Assert
            numbers.Should().BeEmpty();
        }
    }
}
